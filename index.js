const enterTodo = document.getElementById('enter-todo');
const addTodo = document.getElementById('add-todo');
const list = document.querySelector('.list-item');

let arr = [];

if (localStorage.length === 0) {
    localStorage.setItem('todo', "[]");
}

// arr = [...JSON.parse(localStorage.getItem('todo'))]; // Use of spread operator
arr = JSON.parse(localStorage.getItem('todo'));
const createTodo = ((text, id, checked) => {

    const div = document.createElement('div');
    div.classList.add('items');
    if (checked) {
        div.classList.add ('strike');
    }else {
        div.classList.remove ('strike');

    }
    div.id = id;

    const checkBox = document.createElement('input');
    checkBox.setAttribute('type', 'checkbox');
    checkBox.classList.add('checkbox');
    if (checked) {
        checkBox.checked = true;
    }else {
        checkBox.checked = false;
    }
    checkBox.setAttribute('id', id);
    checkBox.addEventListener('change', completeTodo)
    div.appendChild(checkBox);

    const span = document.createElement('span');
    span.classList.add('todo-name');
    span.id = id;
    span.innerText = text;
    div.appendChild(span);

    const deleteTodo = document.createElement('button');
    deleteTodo.setAttribute('id', id);
    deleteTodo.innerText = 'Delete';
    deleteTodo.addEventListener('click', deleteTodoList);
    div.appendChild(deleteTodo);

    const editTodo = document.createElement('button');
    editTodo.setAttribute('id', id);
    editTodo.innerText = 'Edit';
    editTodo.addEventListener ('click', editTodoList);
    div.appendChild(editTodo);

    return div;
})

const addList = (() => {
    enterTodo.addEventListener('keypress', ((event) => {
        if (event.keyCode === 13 && enterTodo.value !== '') {
            let randId = Math.floor(Math.random() * 10000) + enterTodo.value.substring(0, 3);
            const div = createTodo(enterTodo.value, randId);

            let obj = {
                "id": randId,
                "txt": enterTodo.value,
                "checked": false
            };
            arr.push(obj);
            localStorage.setItem('todo', JSON.stringify(arr));

            list.appendChild(div);
            enterTodo.value = '';
        }
    }))
})

const onLoadPage = (() => {
    let todoItem = JSON.parse(localStorage.getItem('todo'));
    todoItem.forEach((val, index) => {
        let key = val.id;
        let txt = val.txt;
        let checked = val.checked;
        console.log(key, txt);
        const div = createTodo(txt, key, checked);
        list.appendChild(div);
        // enterTodo.value = '';
    })
})

const addTodoBtn = (() => {
    addTodo.addEventListener('click', (() => {
        if (enterTodo.value === '') {
            return;
        }
        let randId = Math.floor(Math.random() * 10000) + enterTodo.value.substring(0, 3);
        const div = createTodo(enterTodo.value, randId);

        let obj = {
            "id": randId,
            "txt": enterTodo.value,
            "checked": false
        };
        arr.push(obj);
        localStorage.setItem('todo', JSON.stringify(arr));

        list.appendChild(div);
        enterTodo.value = '';
    }))
})

const completeTodo = ((event) => {
    const tar = event.target;

    if (tar.parentElement.className !== 'strike' && tar.checked) {
        tar.parentElement.style.textDecoration = 'line-through';
        const getTodo = JSON.parse(localStorage.getItem ('todo'));
        getTodo.forEach ((val, index) => {
            if (tar.parentElement.id === val.id) {
                val.checked = true;
                localStorage.setItem ('todo', JSON.stringify (getTodo));
            }
        })
    }else {
        tar.parentElement.style.textDecoration = 'none';
        const getTodo = JSON.parse(localStorage.getItem ('todo'));
        getTodo.forEach ((val, index) => {
            if (tar.parentElement.id === val.id) {
                val.checked = false;
                localStorage.setItem ('todo', JSON.stringify (getTodo));
            }
        })
    }
})

const deleteTodoList = ((event) => {
    const tar = event.target;
    tar.parentElement.remove();
    let data = JSON.parse(localStorage.getItem('todo'));
    let len = data.length;
    console.log(tar.parentElement.id);
    for (let i = 0; i < len; i++) {
        if (data[i].id === tar.parentElement.id) {
            let deleteItem = arr.splice(i, 1);
            localStorage.setItem('todo', JSON.stringify(arr));
        }
    }
})

const editTodoList = ((event) => {
    // const tar = event.target;
    // tar.parentElement.remove();
    // let data = JSON.parse(localStorage.getItem('todo'));
    // let len = data.length;
    // console.log(tar.parentElement.id);
    // for (let i = 0; i < len; i++) {
    //     if (data[i].id === tar.parentElement.id) {
    //         enterTodo.value = JSON.parse (localStorage.getItem ('todo'))[i]['txt'];
    //         let deleteItem = arr.splice(i, 1);
    //         localStorage.setItem('todo', JSON.stringify(arr));
    //     }
    // }

    // console.log(event.target.parentElement);

    // const input = document.createElement ('input');
    // document.body.appendChild (input);
    // const ullist=document.getElementsByClassName('list-item');
    list.innerHTML="";
    const div = event.target.parentElement;
    console.log(event.target);

    let getData = JSON.parse (localStorage.getItem ('todo'));
    getData.forEach ((val, index) => {
        if (val.id === div.id) {
            const data = prompt ('Edit Todo', val.txt);
            if (data === '') {
                return;
            }
            val.txt = data;
            localStorage.setItem ('todo', JSON.stringify(getData));
        }
    })

    getData = JSON.parse (localStorage.getItem ('todo'));

    getData.forEach ((val) => {
        
        const div = createTodo (val.txt, val.id, val.checked);

        list.appendChild(div);
        enterTodo.value = '';
    })
    // createTodo();

})

onLoadPage();
addTodoBtn();
addList();


